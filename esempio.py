
import random

import string

passwd = string.ascii_lowercase

''.join([random.choice(passwd) for _ in range(10)])
